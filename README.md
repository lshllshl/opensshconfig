# opensshconfig

A parser for open ssh config files:

- /etc/ssh/sshd_config
- /etc/ssh/ssh_config
- ~/.ssh/config

## Intro
OpenSSH config reads, manipulates and generates OpenSSH configuration files.

It was initially developed for mass deployment of servers.

## Installation

You can find the latest code version [here](https://gitlab.com/lshllshl/opensshconfig).

